import React, { useState, useEffect } from "react";
import { Typography, Avatar, Button, Menu } from "antd";
import { Link } from "react-router-dom";
import {
  BulbOutlined,
  HomeOutlined,
  MoneyCollectOutlined,
  FundOutlined,
  MenuOutlined,
} from "@ant-design/icons";

import cryptoIcon from "../images/cryptocurrency.png";

const Navbar = () => {
  const [activeMenu, setActiveMenu] = useState(true);
  const [screenSize, setSreenSize] = useState(null);

  useEffect(() => {
    const handeResize = () => setSreenSize(window.innerWidth);
    window.addEventListener("resize", handeResize);

    handeResize();

    return () => window.removeEventListener("resize", handeResize);
  }, []);

  useEffect(() => {
    if (screenSize < 768) {
      setActiveMenu(false);
    } else {
      setActiveMenu(true);
    }
  }, [screenSize]);

  return (
    <div className="nav-container">
      <div className="logo-container">
        <Avatar src={cryptoIcon} size="large" />
        <Typography.Title level={2} className="logo">
          <Link to="/">Cryptospace</Link>
        </Typography.Title>
        <Button
          className="menu-control-container"
          onClick={() => setActiveMenu(!activeMenu)}
        >
          <MenuOutlined />
        </Button>
      </div>
      {activeMenu && (
        <Menu theme="dark">
          <Menu.Item icon={<HomeOutlined />}>
            <Link to="/">Home</Link>{" "}
          </Menu.Item>
          <Menu.Item icon={<FundOutlined />}>
            <Link to="/cryptocurrencies">Cryptocurrencies</Link>{" "}
          </Menu.Item>
          <Menu.Item icon={<MoneyCollectOutlined />}>
            <Link to="/exchanges">Exchanges</Link>{" "}
          </Menu.Item>
          <Menu.Item icon={<BulbOutlined />}>
            <Link to="/news">News</Link>{" "}
          </Menu.Item>
        </Menu>
      )}
    </div>
  );
};

export default Navbar;
