import React, { useState } from "react";
import moment from "moment";
import { useGetCryptoNewsQuery } from "../services/cryptoNewsApi";
import { useGetCryptosQuery } from "../services/cryptoApi";
import { Loader } from "./";
import { Select, Row, Col, Typography, Card, Avatar } from "antd";

const { Text, Title } = Typography;
const { Option } = Select;

const demoImage =
  "https://coinrevolution.com/wp-content/uploads/2020/06/cryptonews.jpg";

const News = ({ simplified }) => {
  const [newsCategory, setNewsCategory] = useState("Cryptocurrency");
  const { data: cryptos } = useGetCryptosQuery(100);

  const { data: news } = useGetCryptoNewsQuery({
    newsCategory,
    count: simplified ? 6 : 18,
  });

  if (!news?.value) return <Loader />;
  return (
    <>
      <Row gutter={[24, 24]}>
        {!simplified && (
          <Col span={24}>
            <Select
              showSearch
              className="select-news"
              placeholder="Select Crypto"
              optionFilterProp="children"
              onChange={(value) => setNewsCategory(value)}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) > 0
              }
            >
              <Option value="Cryptocurrency">Cryptocurrency</Option>
              {cryptos?.data?.coins.map((coin, idx) => (
                <Option value={coin?.name} key={coin.uuid}>
                  {coin.name}
                </Option>
              ))}
            </Select>
          </Col>
        )}
        {news?.value.map((article, idx) => (
          <Col xs={24} sm={12} lg={8} key={idx}>
            <Card hoverable className="news-card">
              <a href={article.url} rel="noreferrer" target="_blank">
                <div className="news-image-container">
                  <Title className="news-title" level={4}>
                    {article.name}
                  </Title>
                  <img
                    src={article?.image?.thumbnail?.contentUrl || demoImage}
                    alt="Article image"
                    style={{
                      maxWidth: "120px",
                      maxHeight: "100",
                      objectFit: "contain",
                    }}
                  />
                </div>
                <p>
                  {article?.description > 100
                    ? `${article?.description.substring(0, 100)}...`
                    : article?.description}
                </p>
                <div className="provider-container">
                  <div>
                    <Avatar
                      src={
                        article?.provider[0]?.image?.thumbnail?.contentUrl ||
                        demoImage
                      }
                    />
                    <Text className="provider-name">
                      {article?.provider[0]?.name}
                    </Text>
                  </div>
                  <Text>
                    {moment(article.datePublished).startOf("ss").fromNow()}
                  </Text>
                </div>
              </a>
            </Card>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default News;
