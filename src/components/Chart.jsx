import React from "react";
import { Line } from "react-chartjs-2";
import { Col, Row, Typography } from "antd";
import millify from "millify";
import moment from "moment";
import Chart from "chart.js/auto";

const { Title } = Typography;

const CoinChart = ({ coinHistory, currentPrice, coinName }) => {
  const coinPrice = [];
  const coinTimestamp = [];

  for (let i = 0; i < coinHistory?.data?.history?.length; i++) {
    coinPrice.push(coinHistory.data.history[i].price);
    coinTimestamp.push(
      moment.unix(coinHistory?.data?.history[i].timestamp).format("DD-MM-YYYY")
    );
  }

  const data = {
    labels: coinTimestamp,
    datasets: [
      {
        label: "Price in USD",
        data: coinPrice,
        fill: false,
        backgroundColor: "#0071bd",
        borderColor: "#0071bd",
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return (
    <>
      <Row className="chart-header">
        <Title className="chart-title" level={2}>
          {coinName} Price Chart{" "}
        </Title>
        <Col className="price-container">
          <Title className="price-change" level={5}>
            {coinHistory?.data?.change}%
          </Title>
          <Title className="current-price" level={5}>
            Current {coinName} price: ${millify(currentPrice)}
          </Title>
          <Title></Title>
        </Col>
      </Row>
      <Line data={data} options={options} />
    </>
  );
};

export default CoinChart;
