import React from "react";
import millify from "millify";
import { Collapse, Row, Col, Typography, Avatar, Empty } from "antd";
import HTMLReactParser from "html-react-parser";

import { useGetExchangesQuery } from "../services/cryptoApi";
import { Loader } from "./";

const { Text } = Typography;
const { Panel } = Collapse;

const Exchanges = () => {
  const { data, isFetching } = useGetExchangesQuery();
  const exchangesList = data?.data?.exchanges;
  // Note: To access this endpoint you need premium plan
  if (isFetching) return <Loader />;

  return (
    <>
      <Empty
        image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
        imageStyle={{
          height: 60,
        }}
        description={
          <span>
            Unforturnately, to access this endpoint you need a premium plan.
          </span>
        }
        style={{ marginBottom: "544px" }}
      />
    </>
  );
};

export default Exchanges;
